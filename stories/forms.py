from django import forms
import bleach
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.utils.safestring import mark_safe

from stories.models import Story, Tag


class PopularTagsWidget(forms.CheckboxSelectMultiple):
    def render(self, name, value, attrs=None, choices=()):
        self.checked_popular_tags = self.checked_popular_tags or []
        output = []
        id = 'popular_tags_input_{}'
        i = 0
        for tag in self.choices.queryset:
            inp_id = id.format(i)
            checked = 'checked' if tag in self.checked_popular_tags else ''
            output.append('<div class="span-5 last checkbox_2 current">')
            output.append('<input type="checkbox" id="{}" value="{}" name="{}" {}>'
                          .format(inp_id, tag.pk, 'popular_tags', checked))
            output.append('<label for="{}" class="cursor_pointer">{}</label>'.format(inp_id, tag.title))
            output.append('<span class="green_text ">({} stories)</span>'.format(tag.num_stories))
            output.append('</div>')
            i += 1
        return mark_safe('\n'.join(output))


class EditStoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditStoryForm, self).__init__(*args, **kwargs)
        self.checked_popular_tags = []
        if self.instance.pk:
            self.checked_popular_tags = self.instance.tags.filter(popular=True)

        self.fields['popular_tags'] = forms.ModelMultipleChoiceField(
            required = False,
            queryset = Tag.objects.filter(popular=True).annotate(num_stories=Count('stories')),
            widget = PopularTagsWidget(),
        )
        self.fields['popular_tags'].widget.checked_popular_tags = self.checked_popular_tags

        self.fields['tags'] = forms.ModelMultipleChoiceField(
            required = False,
            queryset = Tag.objects.filter(popular=False),
            widget = forms.SelectMultiple(
                attrs = {
                    'data-placeholder':'Or search some category there...',
                    'data-url': reverse('stories:search_tags'),
                }
            )
        )

    class Meta:
        model = Story
        fields = ['title', 'story_text', 'cover', 'anonymous', 'tags']
        widgets={
            'title': forms.TextInput(attrs={'placeholder':'YOU STORY NAME','name':'title','class':'transparent_text2 green_placeholder'}),
            'story_text': forms.Textarea(attrs={'id':'story_textarea'}),
            'anonymous': forms.CheckboxInput(attrs={'id':'anonimous', 'placeholder':'YOU STORY NAME','name':'anonymous'}),
            'cover': forms.FileInput(attrs={'class': 'cursor_pointer'})
        }

    def clean_story_text(self):
        text = self.cleaned_data.get('story_text', '')
        cleaned_text = bleach.clean(text, settings.BLEACH_VALID_TAGS, settings.BLEACH_VALID_ATTRS)
        return cleaned_text

    def clean(self):
        cleaned_data = super(EditStoryForm, self).clean()
        if not cleaned_data.get('popular_tags') and not cleaned_data.get('tags'):
             raise forms.ValidationError({'tags': forms.ValidationError('Need to add tags', code='code')})
        return cleaned_data

    def save(self, commit=False, *args, **kwargs):
        self.cleaned_data['tags'] = list(self.cleaned_data['popular_tags']) + list(self.cleaned_data['tags'])
        return super(EditStoryForm, self).save(*args, **kwargs)