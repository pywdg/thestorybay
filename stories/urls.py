from django.conf.urls import patterns, url
from stories.views import *


urlpatterns = patterns('stories.views',
    url(r'^library/$', Library.as_view(), name='library'),
    url(r'^library/by_tag/(?P<tag_slug>[\w-]+)/$', ByTag.as_view(), name='by_tag'),
    url(r'^hot/$', Hot.as_view(), name='hot'),
    url(r'^feed/$', Feed.as_view(), name='feed'),

    url(r'^detail/(?P<pk>[\d]+)/$', Detail.as_view(), name='detail'),
    url(r'^add/$', Create.as_view(), name='add'),
    url(r'^edit/(?P<pk>\d+)/$', Update.as_view(), name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', delete, name='delete'),

    url(r'^search_tags', search_tags, name='search_tags'),

    url(r'^search_form', 'search_form', name='search_form'),
    url(r'^search', Search.as_view(), name='search'),
)