from math import log

from django.db import models
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from settings.models import LikePower
from django.utils import timezone
from thestorybay.settings import AUTH_USER_MODEL
from sorl.thumbnail import ImageField as ThumbnailImageField


class Tag(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True, blank=True)
    popular = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse(
            'stories:by_tag',
            args=[self.slug])

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name_plural = 'tags'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Tag, self).save(*args, **kwargs)


class Story(models.Model):
    title = models.CharField(max_length = 62)
    story_text = models.TextField()
    created_on = models.DateTimeField(auto_now_add = True)
    rating = models.IntegerField(default = 0)
    amount_views = models.IntegerField(default = 0)
    amount_comments = models.IntegerField(default = 0)
    cover = ThumbnailImageField(upload_to = 'upload/story_covers/%Y/%m/%d')
    author = models.ForeignKey(AUTH_USER_MODEL, related_name='written_stories')
    anonymous = models.BooleanField(default = False)
    tags = models.ManyToManyField(Tag, related_name='stories')

    class Meta:
        ordering = ['-created_on']
        verbose_name_plural = 'stories'

    def is_anonymous(self):
        return self.anonymous

    def get_absolute_url(self):
        return reverse('stories:detail', kwargs={
            'pk':self.id})

    def calc_hot_rating(self):
        EPOCH = timezone.make_aware(timezone.datetime(2014, 1, 1), timezone.utc)
        rating = \
            self.rating * 0.4 + \
            self.amount_views * 0.15 + \
            self.amount_comments * 0.5
        order = log(rating, 10)
        seconds = (self.created_on - EPOCH).total_seconds()
        self.hot_rating = order + seconds / 50000
        return self.hot_rating

    def __str__(self):
        r = str(self.title) + ' - '
        r += str(self.story_text)[:30] + ' - '
        r += str(self.author) + ' - '
        r += self.created_on.strftime('%d.%m.%Y - %H:%M') + ' - '
        r += 'views: ' + str(self.amount_views) + ' - '
        r += 'votes: ' + str(self.rating) + ' - '
        r += 'comments: ' + str(self.amount_comments)
        return r

    def get_username(self):
        if self.anonymous:
            return 'Anonymous'
        return self.author.username


class MetricStoryDetailViews(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL)
    story = models.ForeignKey(Story)
    date = models.DateTimeField(auto_now_add = True)

    def save(self, *args, **kwargs):
        if self.user != self.story.author:
            if not MetricStoryDetailViews.objects.filter(
                user=self.user, story=self.story):
                super(MetricStoryDetailViews, self).save(*args, **kwargs)
                return True
        return False