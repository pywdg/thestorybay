from django.dispatch import receiver
from django.db.models.signals import post_save

from stories.models import MetricStoryDetailViews


@receiver(post_save, sender=MetricStoryDetailViews)
def increment_story_views(sender, instance, created, raw, **kwargs):
    if created:
        instance.story.amount_views += 1
        instance.story.save()