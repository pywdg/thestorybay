$(function(){
    $('#found_block').endlessPaginate({
        paginateOnScroll: true,
        paginateOnScrollMargin: 350,
        paginateOnScrollElem: $('.arcticmodal-container'),
        paginateOnScrollElemHeight: $('.arcticmodal-container_i')
    });
});