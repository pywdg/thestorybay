function formatResult(item) {
    if(!item.id) {
        return item.title;
    }
    return '<i>' + item.title + '</i>';
}

function formatSelection(item) {
    return '<b>' + item.text + '</b>';
}

$(function () {
    $('#edit_story_form_next').on('click', function(e){
        e.preventDefault();
        $('#mod_add_story_form_content1').hide();
        $('#mod_add_story_form_content2').show();
    });

    $('#edit_story_form_delete').on('click', function(e){
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: $('#edit_story_form_delete').data('delete_url'),
            //data: $('#edit_story_form').serialize(),
            success: function(data) {if (data.redirect_url) {$(location).attr('href', data.redirect_url)}},
            error:  function(xhr, str){}
        });
    });

    $('#story_textarea').ckeditor();

    var id_tags = $('#id_tags');
    id_tags.select2({
        ajax: {
            url: id_tags.data('url'),
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, page) {
                return {results: data.items};
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: formatResult,
        templateSelection: formatSelection
    });
});