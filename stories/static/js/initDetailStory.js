$(function () {

    FB.init({
        appId: '1593925587488543',
        version: 'v2.3'
    });

    add_comment_reply_form = $('#add_comment_reply_form');
    comment_pk_input = add_comment_reply_form.find('#comment_pk');

    $(document).on('click', '.reply_comment', function(e){
        e.preventDefault();
        if ('comment_text_reply' in CKEDITOR.instances) CKEDITOR.instances['comment_text_reply'].destroy();
        var elem = $(this);
        comment_pk_input.val(elem.data('comment'));
        elem.before(add_comment_reply_form);
        $('#comment_text_reply').ckeditor();
        $(elem,'.reply_comment').hide();
        add_comment_reply_form.show();
    });

    $(document).on('click', '.reply_comment_cancel', function(e) {
        e.preventDefault();
        $('.reply_comment').show();
        add_comment_reply_form.fadeOut();
    });


    $('.fb_init_share_dialog').on('click', function(){
        var elem = $(this);
        FB.ui({
            method: 'feed',
            link: elem.data('link'),
            caption: elem.data('caption'),
            picture: elem.data('picture'),
            description: elem.data('description')
        }, function(response){});
    });
});