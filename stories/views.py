from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import redirect, get_object_or_404, render
from django.templatetags.static import static
from django.views.generic import CreateView, UpdateView
from endless_pagination.views import AjaxListView

from comments.models import Comment
from common.utils import AjaxForm, JsonView
from stories.models import Story, MetricStoryDetailViews, Tag
from stories.forms import EditStoryForm
from comments.forms import AddCommentForm, AddCommentReplyForm
from thestorybay.settings import BASE_REDIRECT


class Library(AjaxListView):
    model = Story
    context_object_name = "stories"
    template_name = 'stories/story_list.html'
    page_template = 'stories/story_list_page.html'

    def get_context_data(self, **kwargs):
        context = super(Library, self).get_context_data(**kwargs)
        context['preview_template'] = 'stories/parts/preview.html'
        return context


class Hot(Library):
    def get_queryset(self):
        return sorted(Story.objects.filter(rating__gt=0), key = lambda story: -story.calc_hot_rating())


class ByTag(Library):
    def get_queryset(self):
        tag = get_object_or_404(Tag, slug=self.kwargs['tag_slug'])
        return Story.objects.filter(tags=tag)


class Feed(Library):
    def get_queryset(self):
        followings = self.request.user.followings.all()
        stories = Story.objects.filter(
            Q(bookmark_users=followings) |
            Q(author=followings, anonymous=False) |
            Q(votes=followings)
        ).distinct()
        for story in stories:
            vote_users = [vote.user for vote in story.votes.all()]
            bookmark_users = story.bookmark_users.all()
            story.my_following_bookmark_users = list(set(followings).intersection(set(bookmark_users)))
            story.my_following_vote_users = list(set(followings).intersection(set(vote_users)))
        return stories


class Search(JsonView, Library):
    context_to_json = ['count_stories']

    def get_context_data(self, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        context['count_stories'] = len(context['object_list'])
        context['preview_template'] = 'stories/parts/preview.html'
        return context

    def get_queryset(self):
        return Story.objects.filter(title__icontains=self.request.GET['q'])


def search_form(request):
    return render(request, 'stories/modals/search_modal.html', {
        'additional_scripts': [static('js/initSearch.js')]
    })


class Detail(AjaxListView):
    model = Comment
    context_object_name = "comments"
    template_name = 'stories/story_detail.html'
    page_template = 'comments/branch_page.html'

    def get_queryset(self):
        return self.story.comment_set.filter(to_comment=None)

    def dispatch(self, *args, **kwargs):
        self.story = get_object_or_404(Story, pk=kwargs['pk'])
        MetricStoryDetailViews.objects.get_or_create(user = self.request.user, story = self.story)
        return super(Detail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Detail, self).get_context_data(**kwargs)
        context['additional_scripts'] = [
            static('js/initDetailStory.js'),
            static('js/sdk.js')
        ]
        context['form_action'] = reverse('comments:add', kwargs={'story_pk': self.story.pk})
        reply_form = AddCommentReplyForm()
        reply_form.close_btn = True
        context['forms'] = [AddCommentForm(), reply_form]
        context['story'] = get_object_or_404(Story, pk=self.kwargs.get('pk'))
        return context


class Create(AjaxForm, CreateView):
    model = Story
    fields = ['title', 'text', 'cover', 'anonymous', 'category']
    template_name = 'stories/modals/edit.html'
    form_class = EditStoryForm

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        context['additional_scripts'] = [
            static('select2/js/select2.min.js'),
            static('js/initEditStoryForm.js')
        ]
        context['additional_css'] = [
            static('select2/css/select2.min.css'),
        ]
        context['form_action'] = reverse('stories:add')
        return context

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(Create, self).form_valid(form)


class Update(Create, UpdateView):
    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['form_action'] = reverse('stories:edit', kwargs={'pk':self.get_object().id})
        context['form_delete'] = reverse('stories:delete', kwargs={'pk':self.get_object().id})
        return context


def delete(request, pk):
    story = get_object_or_404(Story, pk=pk)
    redirect_url = story.author.get_absolute_url()
    if story.author == request.user:
        story.delete()
    if request.is_ajax():
        return JsonResponse({'redirect_url': redirect_url})
    return redirect(redirect_url)


def search_tags(request):
    if request.is_ajax():
        tags = Tag.objects.filter(
             Q(title__icontains=request.GET['q']) &
             Q(popular=False)
        )
        data = {}
        data['items'] = [{'id': tag.pk, 'title': tag.title } for tag in tags]
        return JsonResponse(data)
    return redirect(reverse(BASE_REDIRECT))