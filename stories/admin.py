from django.contrib import admin
from stories.models import Story, Tag

admin.site.register(Story)
admin.site.register(Tag)