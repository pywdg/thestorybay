from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect

from comments.models import Comment
from stories.models import Story
from thestorybay.settings import BASE_REDIRECT
from voting.models import StoryVote, CommentVote


def vote(request, object, pk):
    if not request.is_ajax() or request.method != 'POST': return redirect(reverse(BASE_REDIRECT))
    user = request.user
    direction = request.POST.get('direction')
    if object == 'story':
        instance = get_object_or_404(Story, pk=pk)
        vote = StoryVote.objects.get_or_create(user=user, story=instance)[0]
    elif object == 'comment':
        instance = get_object_or_404(Comment, pk=pk)
        vote = CommentVote.objects.get_or_create(user=user, comment=instance)[0]
    else:
        return JsonResponse({
            'status': 'error',
            'message': 'Unknown object'
        })
    try:
        like = vote.register(instance.author, direction)
    except Exception as e:
        return JsonResponse({
            'status': 'warning',
            'message': str(e)
        })
    vote.update_rating([instance.author, instance])
    return JsonResponse({
        'pk': pk,
        'object': object,
        'status': 'success',
        'message': 'Your vote counted',
        'like': like,
        'author_pk': instance.author.pk
    })