from django.db import models
from comments.models import Comment
from settings.models import LikePower
from stories.models import Story
from thestorybay.settings import AUTH_USER_MODEL, VOTEABLE_TIME
from django.utils import timezone


class Vote(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL)
    created_on = models.DateTimeField(auto_now_add=True)
    value = models.SmallIntegerField(default=0)

    def is_voteable(self):
        seconds = (timezone.now() - self.created_on).total_seconds()
        if seconds > VOTEABLE_TIME:
            return False
        return True

    def change_value(self, like):
        if self.value == -like:
            val = 0
        elif self.value == 0:
            val = like
        elif self.value == like:
            raise Exception('Already max votes')
        self.value = val
        self.save()
        return True

    def register(self, instance_owner, direction):
        if instance_owner == self.user:
            self.delete()
            raise Exception("It's your story")
        if not self.is_voteable(): raise Exception('Time for voting is end')

        like_power = LikePower.get_for_rating(self.user.rating)
        self.like = like_power if direction == 'up' else -like_power
        if self.change_value(self.like):
            return self.like

    def update_rating(self, instances_increase_rating):
        for instance in instances_increase_rating:
            instance.rating += self.like
            instance.save()

    class Meta:
        abstract = True


class StoryVote(Vote):
    story = models.ForeignKey(Story, related_name='votes')

class CommentVote(Vote):
    comment = models.ForeignKey(Comment, related_name='votes')