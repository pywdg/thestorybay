from django.conf.urls import patterns, url


urlpatterns = patterns('voting.views',
    url(r'^vote/(?P<object>story|comment)/(?P<pk>[\d]+)/$', 'vote', name='vote'),
)