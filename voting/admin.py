from django.contrib import admin

from voting.models import CommentVote, StoryVote


admin.site.register(StoryVote)
admin.site.register(CommentVote)