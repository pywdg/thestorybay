import os
import socket


if os.environ.get('ENVIRONMENT') == 'dev':
    from thestorybay.develop_settings import *
else:
    from thestorybay.production_settings import *
try:
    from thestorybay.local_settings import *
except:
    pass

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = '2go*7wxk03lh9!08(e3x855*g)ug!!$!6xz96gt2st&k^v1e06'

TEMPLATE_DEBUG = DEBUG

ENABLE_LDAP_SUPPORT = False

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS = PRE_INSTALLED_APPS + (
    # system
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # other
    'social.apps.django_app.default',
    'endless_pagination',
    'sorl.thumbnail',
    'compressor',

    # thestorybay
    'common',
    'settings',
    'stories',
    'accounts',
    'comments',
    'voting',

) + POST_INSTALLED_APPS

MIDDLEWARE_CLASSES += (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
    'django.core.context_processors.request',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
    os.path.join(BASE_DIR, 'stories/templates/'),
    os.path.join(BASE_DIR, 'comments/templates/'),
)

ROOT_URLCONF = 'thestorybay.urls'

WSGI_APPLICATION = 'thestorybay.wsgi.application'

AUTH_USER_MODEL = 'accounts.CustomUser'


# Internationalization
LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True

DATE_FORMAT = 'd.m.Y'
DATETIME_FORMAT = 'd.m.Y - H:i:s  O'
TIME_FORMAT = 'H:i:s'


# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'common', 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = '/media/'

BASE_REDIRECT = 'stories:hot'
VOTEABLE_TIME = 86400 #86400 sec = 1 day

# python social auth
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/'

SOCIAL_AUTH_USER_MODEL = 'accounts.CustomUser'

SOCIAL_AUTH_PIPELINE = (
   'social.pipeline.social_auth.social_details',
   'social.pipeline.social_auth.social_uid',
   'social.pipeline.social_auth.auth_allowed',
   'social.pipeline.social_auth.social_user',
   'social.pipeline.user.get_username',
   'social.pipeline.social_auth.associate_by_email',
   'social.pipeline.user.create_user',
   'social.pipeline.social_auth.associate_user',
   'social.pipeline.social_auth.load_extra_data',
   'accounts.pipelines.save_profile',
   'social.pipeline.user.user_details',
)

SOCIAL_AUTH_TWITTER_KEY = 'Vq4XGs5UdYy8iyMcEZHUURLaw'
SOCIAL_AUTH_TWITTER_SECRET = 'KchXOC3FEsQKVoCFcBEZfsWPlZLIP1VIZOdDET3NeDLiPLFNIW'

SOCIAL_AUTH_FACEBOOK_KEY = '1593925587488543'
SOCIAL_AUTH_FACEBOOK_SECRET = '97d5b3abd57998be47ec18b1e2792f03'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.twitter.TwitterOAuth',
)

AUTHENTICATION_BACKENDS = (
    'social.backends.twitter.TwitterOAuth',
    'social.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

#Endless pagination
ENDLESS_PAGINATION_PER_PAGE = 10

#bleach
BLEACH_VALID_TAGS = ['p', 'b', 'strong', 'em', 'i', 'strike', 'ul', 'li', 'ol', 'br',
                     'span', 'blockquote', 'hr', 'a', 'img', 'u', 's',]
BLEACH_VALID_ATTRS = {
    'span': ['style', ],
    'p': ['align', ],
    'img': ['src', 'alt'],
}

#django-compressor
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',  'compressor.filters.cssmin.CSSMinFilter']