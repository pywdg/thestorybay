from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from stories.models import Story
from accounts.models import CustomUser


def index(request):
    if request.user.is_authenticated():
       return redirect(reverse('stories:library'))
    story = Story.objects.order_by('rating').first()
    return render(request, 'pages/index.html', {
        'story':story })


@login_required
def top(request):
    top_stories = Story.objects.order_by('-rating')[:10]
    top_authors = CustomUser.objects.order_by('-rating')[:10]

    return render(request, 'pages/top.html', {
        'top_stories': top_stories,
        'top_authors': top_authors})