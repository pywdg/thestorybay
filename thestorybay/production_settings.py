# coding=utf-8
from __future__ import unicode_literals

import os

DEBUG = False

ALLOWED_HOSTS = ['*']

PRE_INSTALLED_APPS = ()
POST_INSTALLED_APPS = ()

MIDDLEWARE_CLASSES = ()

TEMPLATE_CONTEXT_PROCESSORS = ()

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

COMPRESS_ENABLED = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'thestorybay_db',
        'USER': 'thestorybay',
        'PASSWORD': '123123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
STATIC_ROOT = os.path.join(BASE_DIR, "static_collected")

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        'django.db': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
    }
}