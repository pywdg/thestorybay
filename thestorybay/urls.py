from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'thestorybay.views.index', name='index'),

    url(r'^accounts/', include(
        'accounts.urls',
        namespace='accounts',
        app_name='accounts')),

    url(r'^stories/', include(
        'stories.urls',
        namespace='stories',
        app_name='stories')),

    url(r'^comments/', include(
        'comments.urls',
        namespace='comments',
        app_name='comments')),

    url(r'^voting/', include(
        'voting.urls',
        namespace='voting',
        app_name='voting')),

    url('', include('social.apps.django_app.urls', namespace='social')),

    url(r'^top$', 'thestorybay.views.top', name='top'),

    url(r'^admin/', include(admin.site.urls)),
)