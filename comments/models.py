from django.db import models
from stories.models import Story
from thestorybay.settings import AUTH_USER_MODEL


class Comment(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    story = models.ForeignKey(Story)
    to_comment = models.ForeignKey('self', blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    comment_text = models.TextField()
    rating = models.IntegerField(default=0)

    class Meta:
        ordering = ['-created_on']