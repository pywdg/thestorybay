from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from comments.models import Comment



@receiver(post_save, sender=Comment)
def increment_story_comments(sender, instance, created, raw, **kwargs):
    if created:
        instance.story.amount_comments += 1
        instance.story.save()

@receiver(post_delete, sender=Comment)
def decrement_story_comments(sender, instance, **kwargs):
    instance.story.amount_comments -= 1
    instance.story.save()
