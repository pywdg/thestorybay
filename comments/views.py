from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.views.generic import CreateView

from common.utils import AjaxForm
from stories.models import Story
from comments.forms import AddCommentForm
from comments.models import Comment


class Create(AjaxForm, CreateView):
    model = Comment
    template_name = 'comments/add.html'
    form_class = AddCommentForm

    def dispatch(self, *args, **kwargs):
        self.story = get_object_or_404(Story, pk=kwargs['story_pk'])
        return super(Create, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return self.story.get_absolute_url()

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.story = self.story
        comment_pk = form.data.get('to_comment')
        if comment_pk: form.instance.to_comment = get_object_or_404(Comment, pk=comment_pk)
        branch_level = int(form.data.get('branch_level'))
        return super(Create, self).form_valid(form, **{
            'render_template': {
                'template':'comments/parts/detail.html',
                'context': {
                    'comment': form.instance,
                    'story': self.story,
                    'branch_level': branch_level,
                    'request': RequestContext(self.request)
                },
            },
            'status': 'success',
            'message': 'Your comment added'
        })