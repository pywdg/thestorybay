from django.conf.urls import patterns, url
from comments.views import Create


urlpatterns = patterns('comments.views',
    url(r'^add/story/(?P<story_pk>[\d]+)/$',
        Create.as_view(),
        name='add'),
    )