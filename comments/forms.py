from django import forms
from django.forms import ModelForm
import bleach
from django.conf import settings

from comments.models import Comment


class AddCommentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddCommentForm, self).__init__(*args, **kwargs)
        self.id = 'add_comment_form'

    class Meta:
        model = Comment
        fields = ('comment_text',)
        widgets={
            'comment_text':forms.Textarea(attrs={'id':'comment_text', 'class':'wysiwyg'}),
        }

    def clean_comment_text(self):
        comment_text = self.cleaned_data.get('comment_text', '')
        cleaned_text = bleach.clean(comment_text, settings.BLEACH_VALID_TAGS, settings.BLEACH_VALID_ATTRS)
        return cleaned_text


class AddCommentReplyForm(AddCommentForm):
    def __init__(self, *args, **kwargs):
        super(AddCommentForm, self).__init__(*args, **kwargs)
        self.id = 'add_comment_reply_form'

    class Meta(AddCommentForm.Meta):
        fields = ('comment_text', 'to_comment')
        widgets={
            'comment_text':forms.Textarea(attrs={'id':'comment_text_reply', 'class':'wysiwyg'}),
            'to_comment': forms.HiddenInput(attrs={'id': 'comment_pk'})
        }