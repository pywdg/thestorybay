from requests import request
from django.core.files.base import ContentFile


def save_profile(backend, user, response, is_new,  *args, **kwargs):
    image_url = None

    uid = response.get('id')
    if backend.name == 'facebook':
        image_url = 'http://graph.facebook.com/{}/picture'.format(uid)
    elif backend.name == "twitter":
        if response.get('profile_image_url'):
            image_url = response.get('profile_image_url')
    if image_url:
        try:
            response = request('GET', image_url, params={'type': 'large'})
            response.raise_for_status()
        except ConnectionError: pass
        user.avatar.save(user.username, ContentFile(response.content), save=False)
    user.save()