from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import logout as django_logout
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import UpdateView
from endless_pagination.views import AjaxListView

from accounts.forms import EditDescription
from accounts.models import CustomUser
from common.utils import AjaxForm
from stories.models import Story
from thestorybay.settings import BASE_REDIRECT


def logout(request):
    django_logout(request)
    return redirect(reverse(BASE_REDIRECT))


class Detail(AjaxListView):
    model = Story
    context_object_name = "stories"
    template_name = 'accounts/detail.html'
    page_template = 'stories/story_list_page.html'

    def get_context_data(self, **kwargs):
        context = super(Detail, self).get_context_data(**kwargs)
        author = get_object_or_404(CustomUser, pk=self.kwargs['pk'])
        if self.request.user == author:
            stories = Story.objects.filter( Q(bookmark_users=author) | Q(author=author) ).all()
        else:
            stories = Story.objects.filter( Q(bookmark_users=author) | Q(author=author, anonymous=False) ).all()
        context['stories'] = stories
        context['preview_template'] = 'stories/parts/preview.html'
        context['form_description'] = EditDescription(instance=author)
        context['author'] = author
        return context


class UpdateDescription(AjaxForm, UpdateView):
    model = CustomUser
    fields = ['description']
    form_class = EditDescription

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return self.object.get_absolute_url()


def sign_in(request):
    return render(request, 'accounts/modals/sign_in_modal.html')


def get_following_followers(request, pk, who):
    author = get_object_or_404(CustomUser, pk=pk)
    return render(request, 'accounts/modals/followings_followers.html', {
        'user': author,
        'who': who
    })


def add_bookmark(request):
    if request.is_ajax() and request.method == 'POST':
        story_pk = int(request.POST.get('story_pk'))
        story = get_object_or_404(Story, pk=story_pk)
        if request.user == story.author:
            return JsonResponse({
                'status': 'error',
                'message': "It's your story"
            })
        if request.user.bookmarks.filter(pk=story_pk).count() == 0:
            request.user.bookmarks.add(story)
            rendered_content = render_to_string('stories/widgets/add_in_bookmarks.html', RequestContext(request, {
                'story':story,
            }))
            return JsonResponse({
                'status': 'success',
                'message': 'Added to bookmarks',
                'rendered_content': rendered_content
            })
        else:
            return JsonResponse({
                'status': 'error',
                'message': 'Already in bookmarks'
            })
    return redirect(reverse(BASE_REDIRECT))


def follow_unfollow(request):
    if request.is_ajax() and request.method == 'POST':
        try:
            to_user_id = int(request.POST.get('author_pk'))
        except ValueError:
            return HttpResponse()
        from_user = request.user
        to_user = get_object_or_404(CustomUser, pk=to_user_id)
        if from_user.pk != to_user.pk:
            data = {
                'to': to_user.pk,
                'from': from_user.pk,
            }
            if  from_user.followings.filter(pk=to_user.pk).count() > 0:
                from_user.followings.remove(to_user)
                data['result'] = 'unfollow'
                data['message'] = 'You have unsubscribed from this author: {}'.format(to_user.username)
            else:
                from_user.followings.add(to_user)
                data['result'] = 'follow'
                data['message'] = 'You have subscribe to this author: {}'.format(to_user.username)
            data['status'] = 'success'
            rendered_content = render_to_string('accounts/widgets/follow_unfollow_form.html', RequestContext(request, {
                'author': to_user,
            }))
            data['rendered_content'] = rendered_content
            return JsonResponse(data)
    return redirect(reverse(BASE_REDIRECT))