from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.urlresolvers import reverse
from settings.models import LikePower
from stories.models import Story
from sorl.thumbnail import ImageField as ThumbnailImageField


class CustomUserManager(BaseUserManager):
    def create_user(self, username, email=None, password=None):
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(
            password = password,
            username = username
        )
        user.email = email
        user.set_password(password)
        user.is_admin = False
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email=None, password=None):
        user = self.create_user(
            username = username,
            password = password
        )
        user.email = email
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser):
    username = models.CharField(
        max_length = 50,
        unique = True,
        db_index = True)
    email = models.EmailField(blank=True, null=True)
    is_admin = models.BooleanField(default=False)
    rating = models.IntegerField(default=0)
    description = models.CharField(max_length=100, null=True, blank=True)
    link = models.CharField(max_length=255, null=True, blank=True)
    avatar = ThumbnailImageField(upload_to='upload/user_avatars/%Y/%m/%d')
    followings = models.ManyToManyField('self', symmetrical=False, related_name='followers')
    bookmarks = models.ManyToManyField(Story, related_name='bookmark_users')
    objects = CustomUserManager()
    USERNAME_FIELD = 'username'

    @property
    def is_staff(self):
        return self.is_admin

    def get_short_name(self):
        return str(self.username)

    def get_full_name(self):
        return str(self.username)

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def is_superuser(self):
        return False

    def __str__(self):
        return str(self.username)

    def get_absolute_url(self):
        return reverse('accounts:detail', kwargs={
            'pk':self.id})