from django.conf.urls import patterns, url
from accounts.views import UpdateDescription, Detail

urlpatterns = patterns('accounts.views',
    url(r'^logout/$', 'logout', name='logout'),
    url(r'^sign_in', 'sign_in', name='sign_in'),

    url(r'^detail/(?P<pk>\d+)/$', Detail.as_view(), name='detail'),

    url(r'^follow_unfollow/$', 'follow_unfollow', name='follow_unfollow'),
    url(r'^add_bookmark/$', 'add_bookmark', name='add_bookmark'),

    url(r'^(?P<pk>[\d]+)/(?P<who>followers|followings)/$', 'get_following_followers', name='get_following_followers'),

    url(r'^update_description/$', UpdateDescription.as_view(), name='update_description'),
)