from django import forms
import re

from accounts.models import CustomUser


class EditDescription(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['description', 'link']
        widgets = {
            'description': forms.TextInput(attrs={'id': 'author_description'}),
            'link': forms.TextInput(attrs={'id': 'author_link'}),
        }

    def clean_link(self):
        link = self.cleaned_data.get('link', '')
        is_twitter = re.match('[http|https://][twitter.com/][a-zA-Z0-9_\-]+', link)
        is_facebook = re.match('[http|https://][facebook.com/][a-zA-Z0-9_\-]+', link)
        if not is_twitter and not is_facebook:
            raise forms.ValidationError('Wrong link.')
        return link