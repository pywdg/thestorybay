from accounts.models import CustomUser
import os


USERMODEL = CustomUser
AMOUNT_BOTS = 50
DIR_BASE = os.path.abspath('/var/www/thestorybay')
DIR_SOURCES = os.path.join(DIR_BASE, 'botofarm/sources')
DIR_IMAGES = '/media/test_images/'

NAMES_TXT = os.path.join(DIR_SOURCES, 'names.txt')
