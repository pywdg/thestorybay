from django.core.management.base import BaseCommand
import time
import random
from botofarm.Bot import Bot
from botofarm.settings import AMOUNT_BOTS
from stories.models import Story


class Command(BaseCommand):
    def handle(self, *args, **options):
        while 1==1:
            self.random_case()
            time.sleep(0.2)

    def random_case(self):
        bot = Bot(pk=random.choice(range(AMOUNT_BOTS)))
        num_last_stories = random.choice(range(5,30))
        story = random.choice(Story.objects.all()[:num_last_stories])
        LIST_CASES = \
            [bot.view_story] * 30 + \
            [bot.vote] * 15 + \
            [bot.comment] * 7 + \
            [bot.create_story] * 1
        choice = random.choice(LIST_CASES)
        choice() if choice == bot.create_story else choice(story)