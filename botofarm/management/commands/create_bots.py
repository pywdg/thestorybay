from django.core.management.base import BaseCommand
from botofarm.Bot import Bot
from botofarm.settings import NAMES_TXT, AMOUNT_BOTS


class Command(BaseCommand):
    def handle(self, *args, **options):
        amount_bots = int(args[0]) if args else AMOUNT_BOTS
        f = open(NAMES_TXT, 'r')
        f_lines = f.readlines()
        for i in range(amount_bots):
            username = f_lines[i][:-1]
            Bot(username=username)
        f.close()
