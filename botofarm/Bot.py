import random

from django.test import Client
from botofarm.tools import lorem_ipsum
from comments.models import Comment
from stories.models import Story, Vote
from botofarm.settings import *
from botofarm.BotStory import BotStory


class Bot(object):
    total = 0

    def __init__(self, username=None, pk=None):
        if pk:
            self.user = CustomUser.objects.get(pk=pk)
            self.username = self.user.username
            self.password = self.username
        elif username:
            self.username = username.lower()
            self.password = self.username
            self.create()
        else:
            self.user = random.choice(CustomUser.objects.all())
            self.username = self.user.username
            self.password = self.user.username

    def status(self):
        print('\nUsername: ', self.username)
        print('количество ботов: ', Bot.total)

    def create(self):
        u = USERMODEL.objects.get_or_create(username=self.username)[0]
        u.set_password(self.password.lower())
        u.save()
        Bot.total += 1
        self.user = u
        self.status()

    def view_story(self, story):
        client = Client()
        client.login(username=self.username, password=self.password)
        response = client.get(story.get_absolute_url())
        return response

    def vote(self, story):
        response = self.view_story(story)
        if response.status_code == 200:
            direction = random.choice(['up']*2 + ['down'])
            vote = Vote.objects.get_or_create(user=self.user, story=story)[0]
            vote.register(direction=direction)
        else:
            return False

    def comment(self, story):
        response = self.view_story(story)
        if response.status_code == 200:
            story = Story.objects.get(pk=story.pk)
            comment = Comment.objects.create(
                author = self.user,
                story = story,
                text = lorem_ipsum(random.choice(range(1,4))))
            return comment
        return False

    def create_story(self):
        BotStory(self.user)
