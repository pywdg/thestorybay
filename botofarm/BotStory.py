import random

from django.core.files import File
import os
from accounts.models import CustomUser
from botofarm.settings import DIR_IMAGES
from botofarm.tools import lorem_ipsum
from stories.models import Story, Tag, Category
from common.utils import get_random_str


class BotStory():
    def __init__(self, user=None):
        if user:
            self.user = user
        else:
            self.user = CustomUser.objects.get(pk=random.choice(range(5000)))
        self.title = self.get_title()
        self.text = self.get_text()
        self.cover = self.upload_image()
        self.category = random.choice(Category.objects.all())
        self.tags = self.get_tags()
        self.create()

    def get_title(self):
        return get_random_str(20) 

    def get_text(self):
        num_paragraphs = random.choice(range(1, 4))
        return lorem_ipsum(num_paragraphs)

    def upload_image(self):
        img_name = random.choice(os.listdir(DIR_IMAGES))
        img_path = os.path.join(DIR_IMAGES, img_name)
        img = Image(file_img = File(open(img_path, 'rb')), user = self.user)
        img.save()
        return img

    def get_tags(self):
        tags = []
        for i in range(4):
            tags.append(random.choice(Tag.objects.all()))
        return tags

    def create(self):
        new_story = Story.objects.create(
            title = self.title,
            text = self.text,
            author = self.user,
            cover = self.cover,
            category = self.category,
        )
        new_story.tags = self.tags
        new_story.save()
        return new_story
