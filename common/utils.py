from django.http import JsonResponse
from django.template.loader import render_to_string


def time_profiling(fun):
    import time
    def wrapped():
        time1 = time.time()
        fun()
        time2 = time.time()
        print(time2-time1, ' sec')
    return wrapped


class AjaxForm(object):
    def form_invalid(self, form):
        response = super(AjaxForm, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({
                'form_errors': form.errors,
                'status': 'error'
            })
        else:
            return response

    def form_valid(self, form, *args, **kwargs):
        response = super(AjaxForm, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
                'redirect_url': self.get_success_url()
            }
            if 'render_template' in kwargs:
                rt = kwargs.get('render_template')
                render_html = render_to_string(
                    rt.get('template'),
                    rt.get('context')
                )
                if rt.get('request'):
                    render_html['request'] = rt.get('request')
                data['render_html'] = render_html
            if 'status' in kwargs:
                data['status'] = kwargs.get('status')
            if 'message' in kwargs:
                data['message'] = kwargs.get('message')
            return JsonResponse(data)
        else:
            return response


class JsonView(object):
    context_to_json = []

    def get(self, request, *args, **kwargs):
        response = super(JsonView, self).get(request, *args, **kwargs)
        data = {
            'rendered_content': response.rendered_content
        }
        for var in self.context_to_json:
            if var in response.context_data:
                data[var] = response.context_data[var]
        return JsonResponse(data)