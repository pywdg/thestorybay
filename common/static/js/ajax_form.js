$(document).on('click', '.show_modal', function(e) {
    e.preventDefault();
    $.arcticmodal({
        type: 'ajax',
        url: $(this).data('url'),
        overlay: {css: {background: '#fff', opacity: 1}}
    });
});

$(document).on('submit', '.ajax_form', function(e){
    e.preventDefault();
    var form = $(this),
        data = {};
    data.form = form;
    var data_attrs = form.data();
    if (data_attrs.before_event) {
        $(document).trigger(data_attrs.before_event, data);
    }
    if (form.attr('enctype') == "multipart/form-data") {
        content_type = false;
        if (!data.send_data) {
            data.send_data = new FormData(form[0]);
        }
    }
    else {
        content_type = 'application/x-www-form-urlencoded; charset=UTF-8';
        if (!data.send_data) {
            data.send_data = form.serialize();
        }
    }

    $.ajax({
        type: form.attr('method'),
        url: data_attrs.url,
        data: data.send_data,
        success: function(server_data){
            server_data.form = form;
            server_data.data_attrs = data_attrs;
            if (data_attrs.after_event) {
                $(document).trigger(data_attrs.after_event, server_data);
            }
            show_message(server_data);
        },
        error: function(xhr, str){
            $.growl.error({ message: 'Unknown error.' });
        },
        cache: false,
        contentType: content_type,
        processData: false
    });
});