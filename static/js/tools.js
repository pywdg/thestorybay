var get_user_profile = function(pk){
    var elems = $('.user_profile[data-pk='+pk+']');
    if (!elems) {return false}

    var followers_elems = elems.find('.user_profile_followers'),
        followings_elems = elems.find('.user_profile_followings'),
        rating_elems = elems.find('.user_profile_rating'),
        stories_count_elems = elems.find('.user_profile_stories_count');

    return {
        'elemss': elems,
        'followers_elems': followers_elems,
        'followers': parseInt(followers_elems.first().text()),
        'followings_elems': followings_elems,
        'followings': parseInt(followings_elems.first().text()),
        'rating_elems': rating_elems,
        'rating': parseInt(rating_elems.first().text()),
        'stories_count_elemss': stories_count_elems,
        'stories_count': parseInt(stories_count_elems.first().text())
    };
};


var update_user_profile = function(pk, data){
    var user_profile = get_user_profile(pk);
    if (!user_profile){return false;}
    if (data.followers_update) {
        if (user_profile.followers >= 0) {
            user_profile.followers_elems.text(user_profile.followers + data.followers_update);
        }
    }
    if (data.followings_update) {
        if (user_profile.followings >= 0) {
            user_profile.followings_elems.text(user_profile.followings + data.followings_update);
        }
    }
    if (data.like)
        user_profile.rating_elems.text(user_profile.rating + data.like);
};


var get_story_info = function(pk){
    var elems = '';
    pk = pk || null;
    if (pk)
        elems = $('.story_info[data-pk=' + pk + ']');
    else
        elems = $('.story_info');

    if (!elems) {return false}

    var amount_views_elems = elems.find('.story_amount_views'),
        rating_elemss = elems.find('.story_rating'),
        amount_comments_elems = elems.find('.story_amount_comments');

    return {
        'elems': elems,
        'rating_elemss': rating_elemss,
        'rating': parseInt(rating_elemss.first().text()),
        'amount_views_elems': amount_views_elems,
        'amount_views': parseInt(amount_views_elems.first().text()),
        'amount_comments_elems': amount_comments_elems,
        'amount_comments': parseInt(amount_comments_elems.first().text())
    };
};


var update_story_info = function(pk, data){
    var story_info = get_story_info(pk);
    if (!story_info){return false;}
    if (data.like){
        story_info.rating_elemss.text(story_info.rating + data.like);
    }
    if (data.inc_comments_amount){
        story_info.amount_comments_elems.text(story_info.amount_comments + 1);
    }
};


var get_comment_info = function(pk){
    var elems = $('.comment[data-pk='+pk+']');
    if (!elems) {return false}

    var rating_elems = elems.find('.comment_rating_amount');

    return {
        'elems': elems,
        'rating_elems': rating_elems,
        'rating': parseInt(rating_elems.first().text())
    };
};


var update_comment_info = function(pk, data){
    var comment_info = get_comment_info(pk);
    if (!comment_info){return false;}
    if (data.like){
        comment_info.rating_elems.text(comment_info.rating + data.like);
    }
};

var show_message = function(data) {
    if (data.message) {
        if (data.status == 'success') {
            $.growl.notice({message: data.message});
        }
        else if (data.status == 'warning') {
            $.growl.warning({message: data.message});
        }
        else if (data.status == 'error') {
            $.growl.error({message: data.message});
        }
    }
};