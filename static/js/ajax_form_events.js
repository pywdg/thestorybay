$(document).on('after_bookmarks', function(e, data){
    if (data.status == 'success') {
        html_block = data.form.parent('.in_bookmarks_block').parent();
        html_block.html(data.rendered_content);
    }
});


$(document).on('after_follow_unfollow', function(e, data){
    if (data.status == 'success') {
        var count;
        if (data.result == 'follow') {count = 1;}
        else if (data.result == 'unfollow') {count = -1;}
        update_user_profile(data.from, {'followings_update':count});
        update_user_profile(data.to, {'followers_update':count});
        html_block = data.form.parent();
        html_block.html(data.rendered_content);
    }
});


$(document).on('before_search', function(e, data){
    $('#found_block').html('');
});

$(document).on('after_search', function(e, data){
    var found_info = $('#found_info');
    found_info.find('#story_count').text(data.count_stories);
    found_info.show();
    $('#found_block').append(data.rendered_content);
});


$(document).on('before_edit_story', function(e, data){
    CKEDITOR.instances.story_textarea.updateElement();
    $('.error_labeles').hide();
    $('#id_title').removeClass('red_transparent_text2 red_placeholder').addClass('transparent_text2 green_placeholder');
    $('#tags_header').addClass('transparent_text green_placeholder').removeClass('red_transparent_text red_placeholder');
    $('#cke_story_textarea').removeClass("invalid_text_area");
    $('#story_cover_btn').removeClass("invalid_story_cover_btn");
});

$(document).on('after_edit_story', function(e, data){
    if (data.redirect_url) {
        $(location).attr('href', data.redirect_url);
    }
    if (data.status == 'error') {
        if ('cover' in data.form_errors || 'title' in data.form_errors || 'story_text' in data.form_errors) {
            $('#mod_add_story_form_content1').show();
            $('#mod_add_story_form_content2').hide();
            $.each(data.form_errors, function (key, value) {
                id = '#err_' + key;
                var elem = $(id);
                elem.text(data.form_errors[key]).show();
                if('title' in data.form_errors) {
                    $('#id_title').removeClass('transparent_text2 green_placeholder').addClass('red_transparent_text2 red_placeholder');
                }
                if('story_text' in data.form_errors) {
                    $('#cke_story_textarea').addClass("invalid_text_area");
                }
                if('cover' in data.form_errors) {
                    $('#story_cover_btn').addClass("invalid_story_cover_btn");
                }
            });
        }
        if ('tags' in data.form_errors) {
            $('#err_tags').text(data.form_errors.tags).show();
            $('#tags_header').removeClass('transparent_text green_placeholder').addClass('red_transparent_text red_placeholder');
        }
    }

});


$(document).on('before_comment', function(e, data){
    var parent_comment = data.form.parent('.comment'),
    new_branch_level;
    if (parent_comment.length) {
        new_branch_level = parent_comment.data('branch_level') + 1;
    }
    else {
        new_branch_level = 1;
    }
    data.send_data = data.form.serialize() + '&branch_level=' + new_branch_level;
});

$(document).on('after_comment', function(e, data){
    var parent_comment = data.form.parent('.comment');
    var textarea = data.form.find('.wysiwyg');
    if (data.status == 'error') {
        if ('comment_text' in data.form_errors) {
            $('#cke_comment_text_reply').addClass("invalid_text_area");
        }
    }
    if (data.render_html) {
        var new_comment = $(data.render_html);
        if (parent_comment.length) {
            parent_comment.after(new_comment);
        }
        else {
            $('#comment_block').prepend(new_comment);
        }
        textarea.val('');
        var instance_name = textarea.attr('id');
        if (instance_name in CKEDITOR.instances) {
            CKEDITOR.instances[instance_name].setData('');
        }
        $('#add_comment_reply_form').slideUp();
        data['inc_comments_amount'] = true;
        update_story_info(pk=null, data);
        $('.reply_comment').show();
        new_comment.animate({ backgroundColor: "#ddffdd"}, 2000).animate({ backgroundColor: "white"}, 1700);
    }
});