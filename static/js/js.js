$(function(){
    $('.ajax_paginate').endlessPaginate({
            paginateOnScroll: true,
            paginateOnScrollMargin: 350
    });

    $('#author_description, #author_link').on('change', function(e){
        $(this).trigger('submit');
    });

    $(document).on('click', '.send_vote', function(e){
        var btn = $(this),
            form = btn.parents('form'),
            result = form.serialize() + '&' + encodeURI(btn.attr('name')) + '=' + encodeURI(btn.attr('value'));
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: form.data('url'),
            data: result,
            success: function(data){
                if (data.object == 'story') update_story_info(data.pk, data);
                if (data.object == 'comment') update_comment_info(data.pk, data);
                update_user_profile(data.author_pk, data);
                show_message(data);
            },
            error:  function(xhr, str){}
        });
    });

    $( '.wysiwyg' ).ckeditor();
    $( '.story_editor' ).ckeditor();
    $( '.comment_editor' ).ckeditor();
    $( '.comment_reply_editor' ).ckeditor();
});