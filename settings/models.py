from django.db import models


class LikePower(models.Model):
    likes_power = models.PositiveSmallIntegerField()
    rating = models.IntegerField()

    @classmethod
    def get_for_rating(self, rating):
        try:
            power = self.objects.order_by('-likes_power') \
                .filter(rating__lte=rating)[0]
            power = power.likes_power
        except IndexError:
            power = 1
        return power

    def __str__(self):
        return 'power = {}, rating = {}'.format(str(self.likes_power), str(self.rating))